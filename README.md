ansible-role-fqdn
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-fqdn/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-fqdn/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Sets FQDN and hostname in Debian-like systems.

Requirements
------------
None

Suported Platforms
--------------
Debian 9 (Stretch)

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: workstations
  become: True
  roles:
    - { role: ansible-role-fqdn, fqdn_full: "web.testdomain.com", fqdn_short: "web"}
```

License
-------

BSD
